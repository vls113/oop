﻿using System;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Tank tank1 = new Tank();
            tank1.Model = "Merkava MK.1";

            Tank tank2 = new Tank();
            tank2.ShowModel();

            Console.WriteLine(tank1._x + " " + tank2.Y + " " + tank1.Z);
        }        
    }

    class Tank 
    {
        public string Model;
        public int _x { get; private set; }
        private int _y;
        public int Y 
        {
            get 
            {
                return _y;
            }
            set 
            {
                _y = value;
            }
        }
        private int _z;
        public int Z => _z;

        public void ShowModel() 
        {
            Console.WriteLine(Model);
        }
    }
}
